use std::rc::Rc;

use crate::observers::DisplayApparatus;

pub trait Subject {
    fn add_observer(&mut self, da: Rc<dyn DisplayApparatus>);
    fn notify(&self);
    fn remove_observer(&mut self, da: Rc<dyn DisplayApparatus>);
    fn set_measurements(&mut self, temperature: f64, humidity: f64, pressure: f64);
}

pub trait StationData {
    fn get_temperature(&self) -> f64;
    fn get_humidity(&self) -> f64;
    fn get_pressure(&self) -> f64;
}

#[derive(Default)]
pub struct WeatherStation {
    observers: Vec<Rc<dyn DisplayApparatus>>,
    pub temperature: f64,
    pub humidity: f64,
    pub pressure: f64,
}

impl Subject for WeatherStation {
    fn add_observer(&mut self, display_apparatus: Rc<dyn DisplayApparatus>) {
        self.observers.push(display_apparatus);
    }

    fn notify(&self) {
        for observer in &self.observers {
            let _ = observer.update();
        }
    }

    fn remove_observer(&mut self, da: Rc<dyn DisplayApparatus>) {
        self.observers.retain(|el| Rc::ptr_eq(el, &da) == false);
    }

    fn set_measurements(&mut self, temperature: f64, humidity: f64, pressure: f64) {
        self.temperature = temperature;
        self.humidity = humidity;
        self.pressure = pressure;
    }
}

impl StationData for WeatherStation {
    fn get_temperature(&self) -> f64 {
        self.temperature
    }
    fn get_humidity(&self) -> f64 {
        self.humidity
    }
    fn get_pressure(&self) -> f64 {
        self.pressure
    }
}
