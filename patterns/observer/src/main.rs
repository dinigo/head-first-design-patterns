mod observers;
mod subject;

use crate::observers::{
    CurrentConditionsDisplay, DisplayApparatus, DisplayStatistics, ForecastDisplay,
    ThirdPartyDisplay,
};
use crate::subject::{Subject, WeatherStation};
use std::cell::RefCell;
use std::ops::Deref;
use std::rc::Rc;

/**
Learnings:
1. A function sould only do one thing, either "organise" other functions or
    "transform" data. This led to set_measurements needing the mut reference to
    the weather station, and calling notify() afterwards, which requires an
    immutable borrow. You can either have multiple borrows or a single
    borrow_mut, but not both.
2. Supertraits to alias a group of interfaces
3. Rc for shared refs, RefCell for interior mutability. <Rc<RefCell<dyn MyTrait>>>
    is a mutable shared reference that implements my trait and it's checked
    dynamically.
 */
fn main() {
    let boxed_station: Rc<RefCell<WeatherStation>> =
        Rc::new(RefCell::new(WeatherStation::default()));
    let ds_observer: Rc<dyn DisplayApparatus> = Rc::new(DisplayStatistics {
        subject: Rc::clone(&boxed_station),
    });
    let cc_observer: Rc<dyn DisplayApparatus> = Rc::new(CurrentConditionsDisplay {
        subject: Rc::clone(&boxed_station),
    });
    let fd_observer: Rc<dyn DisplayApparatus> = Rc::new(ForecastDisplay {
        subject: Rc::clone(&boxed_station),
    });
    let td_observer: Rc<dyn DisplayApparatus> = Rc::new(ThirdPartyDisplay {
        subject: Rc::clone(&boxed_station),
    });
    println!("Registering observers");
    for observer in [cc_observer.clone(), ds_observer, fd_observer, td_observer].iter() {
        boxed_station
            .deref()
            .borrow_mut()
            .add_observer(Rc::clone(&observer));
    }
    // notify observers
    println!("Notifying observers");
    boxed_station
        .deref()
        .borrow_mut()
        .set_measurements(80.0, 65.0, 30.4);
    boxed_station.deref().borrow().notify();
    println!("Removing observer");
    boxed_station
        .deref()
        .borrow_mut()
        .remove_observer(Rc::clone(&cc_observer));
    println!("Notifying observers");
    boxed_station
        .deref()
        .borrow_mut()
        .set_measurements(82.0, 70.0, 29.2);
    boxed_station.deref().borrow().notify();
}
