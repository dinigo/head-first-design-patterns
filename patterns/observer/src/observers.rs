use std::cell::RefCell;
use std::rc::Rc;

use crate::subject::{StationData, WeatherStation};

pub trait DisplayElement {
    fn display(&self);
}

pub trait Observer: DisplayElement {
    fn update(&self) {
        println!("display()-> {}", std::any::type_name::<Self>());
        self.display();
    }
}

pub trait DisplayApparatus: DisplayElement + Observer {}

pub struct DisplayStatistics {
    pub subject: Rc<RefCell<WeatherStation>>,
}

impl DisplayElement for DisplayStatistics {
    fn display(&self) {
        println!(
            "Avg/Max/Min temperature = {}/{}/{}",
            self.subject.borrow().get_temperature(),
            self.subject.borrow().get_humidity(),
            self.subject.borrow().get_pressure()
        );
    }
}

impl Observer for DisplayStatistics {}

impl DisplayApparatus for DisplayStatistics {}

pub struct CurrentConditionsDisplay {
    pub subject: Rc<RefCell<WeatherStation>>,
}

impl DisplayElement for CurrentConditionsDisplay {
    fn display(&self) {
        println!(
            "Current conditions: {} F degrees and {}% humidity",
            self.subject.borrow().temperature,
            self.subject.borrow().humidity,
        );
    }
}

impl Observer for CurrentConditionsDisplay {}

impl DisplayApparatus for CurrentConditionsDisplay {}

pub struct ForecastDisplay {
    pub subject: Rc<RefCell<WeatherStation>>,
}

impl DisplayElement for ForecastDisplay {
    fn display(&self) {
        let forecast = match self.subject.borrow().pressure {
            p if p < 29.0 => "Watch out for cooler, rainy weather",
            p if p > 29.0 && p < 30.0 => "Improving weather on the way!",
            _ => "Sunny",
        };
        println!("Forecast: {}", forecast);
    }
}

impl Observer for ForecastDisplay {}

impl DisplayApparatus for ForecastDisplay {}

pub struct ThirdPartyDisplay {
    pub subject: Rc<RefCell<WeatherStation>>,
}

impl DisplayElement for ThirdPartyDisplay {
    fn display(&self) {
        println!("ThirdPartyDisplay");
    }
}

impl Observer for ThirdPartyDisplay {}

impl DisplayApparatus for ThirdPartyDisplay {}
