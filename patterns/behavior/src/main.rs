// Behavior "functional" interfaces
trait Quack {
    fn quack(&self);
}

trait Fly {
    fn fly(&self);
}

trait DuckBehavior {
    fn perform_fly(&self);
    fn perform_quack(&self);
}

#[derive(Default)]
struct Duck<F: Fly, Q: Quack> {
    fly_behavior: F,
    quack_behavior: Q,
}

impl<FlyBehavior: Fly, QuackBehavior: Quack> DuckBehavior for Duck<FlyBehavior, QuackBehavior> {
    fn perform_fly(&self) {
        self.fly_behavior.fly();
    }
    fn perform_quack(&self) {
        self.quack_behavior.quack();
    }
}

//----------------------------------
// OTHER BEHAVIORS
struct FlyWithWings;

struct FlyNoWay;

struct QuackNormal;

struct QuackSqueak;

struct QuackMute;

impl Fly for FlyWithWings {
    fn fly(&self) {
        println!("FlyWithWings")
    }
}

impl Fly for FlyNoWay {
    fn fly(&self) {
        println!("FlyNoWay")
    }
}

impl Quack for QuackNormal {
    fn quack(&self) {
        println!("QuackNormal")
    }
}

impl Quack for QuackSqueak {
    fn quack(&self) {
        println!("QuackSqueak")
    }
}

impl Quack for QuackMute {
    fn quack(&self) {
        println!("QuackMute")
    }
}

fn main() {
    let mallard_duck = Duck {
        fly_behavior: FlyWithWings,
        quack_behavior: QuackNormal,
    };
    let redhead_duck = Duck {
        fly_behavior: FlyWithWings,
        quack_behavior: QuackSqueak,
    };
    let rubber_duck = Duck {
        fly_behavior: FlyNoWay,
        quack_behavior: QuackSqueak,
    };
    let decoy_duck = Duck {
        fly_behavior: FlyNoWay,
        quack_behavior: QuackMute,
    };
    let ducks: Vec<&dyn DuckBehavior> =
        vec![&mallard_duck, &redhead_duck, &rubber_duck, &decoy_duck];
    for duck in ducks {
        duck.perform_fly();
        duck.perform_quack();
    }
}
