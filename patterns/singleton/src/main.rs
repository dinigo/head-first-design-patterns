use std::sync::Mutex;

struct ChocolateBoiler {
    empty: bool,
    boiled: bool,
}
impl ChocolateBoiler {
    fn fill(&mut self) {
        if self.empty {
            println!("Fill the boiler with a milk/chocolate mixture");
            self.empty = false;
            self.boiled = false;
        } else {
            println!("The boiler is already filled");
        }
    }
    fn drain(&mut self) {
        if !self.empty && self.boiled {
            println!("Drain the boiled milk and chocolate");
            self.empty = true;
        } else {
            println!("The boiler is empty or not boiled");
        }
    }
    fn boil(&mut self) {
        if !self.empty && !self.boiled {
            println!("Bring the contents to a boil");
            self.boiled = true;
        } else {
            println!("The boiler is empty or already boiled");
        }
    }
}

static CHOCOLATE_BOILER: Mutex<ChocolateBoiler> = Mutex::new(ChocolateBoiler {
    empty: true,
    boiled: false,
});

fn factory_process() {
    CHOCOLATE_BOILER.lock().unwrap().fill();
    CHOCOLATE_BOILER.lock().unwrap().boil();
    CHOCOLATE_BOILER.lock().unwrap().drain();
}

fn main() {
    let mut threads = vec![];
    for _ in 0..10 {
        let thread = std::thread::spawn(|| {
            factory_process();
        });
        threads.push(thread);
    }
    for thread in threads {
        thread.join().unwrap();
    }
}
