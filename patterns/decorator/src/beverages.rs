use std::fmt::{Display, Formatter};

pub trait ProductDescription {
    fn get_description(&self) -> String;
}

pub trait ProductCost {
    fn cost(&self) -> f64;
}

#[derive(Debug)]
#[allow(dead_code)]
pub enum BeverageSize {
    Tall,
    Grande,
    Venti,
}

impl BeverageSize {
    pub fn value(&self) -> f64 {
        match self {
            BeverageSize::Tall => 0.10,
            BeverageSize::Grande => 0.10,
            BeverageSize::Venti => 0.10,
        }
    }
}
impl Display for BeverageSize {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            BeverageSize::Tall => write!(f, "Tall"),
            BeverageSize::Grande => write!(f, "Grande"),
            BeverageSize::Venti => write!(f, "Venti"),
        }
    }
}

pub trait Beverage: ProductDescription + ProductCost {
    fn get_size(&self) -> &BeverageSize;
}

pub struct HouseBlend {
    pub size: BeverageSize,
}

pub struct DarkRoast {
    pub size: BeverageSize,
}

pub struct Decaf {
    pub size: BeverageSize,
}

pub struct Espresso {
    pub size: BeverageSize,
}

impl ProductDescription for HouseBlend {
    fn get_description(&self) -> String {
        "HouseBlend".to_string()
    }
}

impl ProductCost for HouseBlend {
    fn cost(&self) -> f64 {
        0.89
    }
}

impl Beverage for HouseBlend {
    fn get_size(&self) -> &BeverageSize {
        &self.size
    }
}

impl ProductCost for DarkRoast {
    fn cost(&self) -> f64 {
        0.99
    }
}

impl ProductDescription for DarkRoast {
    fn get_description(&self) -> String {
        "DarkRoast".to_string()
    }
}

impl Beverage for DarkRoast {
    fn get_size(&self) -> &BeverageSize {
        &self.size
    }
}

impl ProductCost for Decaf {
    fn cost(&self) -> f64 {
        1.05
    }
}

impl ProductDescription for Decaf {
    fn get_description(&self) -> String {
        "Decaf".to_string()
    }
}

impl Beverage for Decaf {
    fn get_size(&self) -> &BeverageSize {
        &self.size
    }
}

impl ProductCost for Espresso {
    fn cost(&self) -> f64 {
        1.99
    }
}

impl ProductDescription for Espresso {
    fn get_description(&self) -> String {
        "Espresso".to_string()
    }
}

impl Beverage for Espresso {
    fn get_size(&self) -> &BeverageSize {
        &self.size
    }
}
