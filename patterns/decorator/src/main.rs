mod beverages;
mod condiments;

use crate::beverages::{
    Beverage, BeverageSize, DarkRoast, HouseBlend, ProductCost, ProductDescription,
};
use crate::condiments::{Mocha, Soy, Whip};
use std::rc::Rc;

fn main() {
    let custom_beverage_01 = Whip {
        beverage: Rc::new(Mocha {
            beverage: Rc::new(Mocha {
                beverage: Rc::new(DarkRoast {
                    size: BeverageSize::Tall,
                }),
            }),
        }),
    };
    println!(
        "Beverage: {} {}, Cost: {}",
        custom_beverage_01.get_size(),
        custom_beverage_01.get_description(),
        custom_beverage_01.cost()
    );
    let custom_beverage_02 = Whip {
        beverage: Rc::new(Mocha {
            beverage: Rc::new(Soy {
                beverage: Rc::new(HouseBlend {
                    size: BeverageSize::Venti,
                }),
            }),
        }),
    };
    println!(
        "Beverage: {} {}, Cost: {}",
        custom_beverage_02.get_size(),
        custom_beverage_02.get_description(),
        custom_beverage_02.cost()
    );
}
