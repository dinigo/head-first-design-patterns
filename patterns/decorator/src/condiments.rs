use crate::beverages::{Beverage, BeverageSize, ProductCost, ProductDescription};
use std::rc::Rc;

trait BeverageDecorator: Beverage {
    fn get_inner_beverage(&self) -> Rc<dyn Beverage>;
}

pub struct Milk {
    pub beverage: Rc<dyn Beverage>,
}

pub struct Mocha {
    pub beverage: Rc<dyn Beverage>,
}

pub struct Soy {
    pub beverage: Rc<dyn Beverage>,
}

pub struct Whip {
    pub beverage: Rc<dyn Beverage>,
}

impl ProductCost for Milk {
    fn cost(&self) -> f64 {
        self.beverage.cost() + 0.10
    }
}

impl ProductDescription for Milk {
    fn get_description(&self) -> String {
        format!("{}, Milk", self.beverage.get_description())
    }
}

impl Beverage for Milk {
    fn get_size(&self) -> &BeverageSize {
        &self.beverage.get_size()
    }
}

impl BeverageDecorator for Milk {
    fn get_inner_beverage(&self) -> Rc<dyn Beverage> {
        Rc::clone(&(self.beverage))
    }
}

impl ProductCost for Mocha {
    fn cost(&self) -> f64 {
        self.beverage.cost() + 0.20
    }
}

impl ProductDescription for Mocha {
    fn get_description(&self) -> String {
        format!("{}, Mocha", self.beverage.get_description())
    }
}

impl Beverage for Mocha {
    fn get_size(&self) -> &BeverageSize {
        &self.beverage.get_size()
    }
}

impl BeverageDecorator for Mocha {
    fn get_inner_beverage(&self) -> Rc<dyn Beverage> {
        self.beverage.clone()
    }
}

impl ProductCost for Soy {
    fn cost(&self) -> f64 {
        self.beverage.cost() + self.beverage.get_size().value() + 0.15
    }
}

impl ProductDescription for Soy {
    fn get_description(&self) -> String {
        format!("{}, Soy", self.beverage.get_description())
    }
}

impl Beverage for Soy {
    fn get_size(&self) -> &BeverageSize {
        &self.beverage.get_size()
    }
}

impl BeverageDecorator for Soy {
    fn get_inner_beverage(&self) -> Rc<dyn Beverage> {
        self.beverage.clone()
    }
}

impl ProductCost for Whip {
    fn cost(&self) -> f64 {
        self.beverage.cost() + 0.10
    }
}

impl ProductDescription for Whip {
    fn get_description(&self) -> String {
        format!("{}, Whip", self.beverage.get_description())
    }
}

impl Beverage for Whip {
    fn get_size(&self) -> &BeverageSize {
        &self.beverage.get_size()
    }
}

impl BeverageDecorator for Whip {
    fn get_inner_beverage(&self) -> Rc<dyn Beverage> {
        self.beverage.clone()
    }
}
