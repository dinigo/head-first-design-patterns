// Execise on implementing the abstract factory pattern. Rust is focused on
// composition over inheritance, so the abstract factory pattern. And the
// abstract factory DOES use composition, where each concrete factory decide
// which sub-factory to use. So instead of Overriding the pizza creation method
// you provide a different ingredient on runtime.
//
// One of the goals of OOP is to "bind" behaviour as late as possible. This way
// we can change the behavior of the system on runtime.


use crate::ingredients::{
    Cheese, Dough, Meat, Sauce, SpanishIngredientFactory, Vegetable,
    IngredientFactory, ItalianIngredientFactory, AmericanIngredientFactory,
};

pub struct Pizza {
    name: String,
    dough: Dough,
    sauce: Sauce,
    cheese: Option<Cheese>,
    meat: Option<Meat>,
    vegetable: Option<Vegetable>,
}

impl Pizza {
    fn prepare(&self) {
        println!("Preparing {}", self.name);
    }
    fn bake(&self) {
        println!("Baking {}", self.name);
    }
    fn cut(&self) {
        println!("Cutting {}", self.name);
    }
    fn box_up(&self) {
        println!("Boxing {}", self.name);
    }
}

pub enum PizzaType {
    Cheese,
    Pepperoni,
    Vegetable,
}


pub trait PizzaFactory {
    fn get_ingredient_factory(&self) -> Box<dyn IngredientFactory>;
    fn create_pizza(&self, pizza_type: PizzaType) -> Pizza {
        let ingredient_factory = self.get_ingredient_factory();
        match pizza_type {
            PizzaType::Cheese => Pizza {
                name: "Spanish Cheese Pizza".to_string(),
                dough: ingredient_factory.create_dough(),
                sauce: ingredient_factory.create_sauce(),
                cheese: Some(ingredient_factory.create_cheese()),
                meat: None,
                vegetable: None,
            },
            PizzaType::Pepperoni => Pizza {
                name: "Spanish Pepperoni Pizza".to_string(),
                dough: ingredient_factory.create_dough(),
                sauce: ingredient_factory.create_sauce(),
                cheese: Some(ingredient_factory.create_cheese()),
                meat: Some(ingredient_factory.create_meat()),
                vegetable: None,
            },
            PizzaType::Vegetable => Pizza {
                name: "Spanish Vegetable Pizza".to_string(),
                dough: ingredient_factory.create_dough(),
                sauce: ingredient_factory.create_sauce(),
                cheese: Some(ingredient_factory.create_cheese()),
                meat: None,
                vegetable: Some(ingredient_factory.create_vegetable()),
            },
        }
    }
    fn order_pizza(&self, pizza_type: PizzaType) -> Pizza {
        let pizza = self.create_pizza(pizza_type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        return pizza;
    }
}

pub struct SpanishPizzeria;

impl PizzaFactory for SpanishPizzeria {
    fn get_ingredient_factory(&self) -> Box<dyn IngredientFactory> {
        Box::new(SpanishIngredientFactory)
    }
}

pub struct ItalianPizzeria;

impl PizzaFactory for ItalianPizzeria {
    fn get_ingredient_factory(&self) -> Box<dyn IngredientFactory> {
        Box::new(ItalianIngredientFactory)
    }
}

pub struct AmericanPizzeria;

impl PizzaFactory for AmericanPizzeria {
    fn get_ingredient_factory(&self) -> Box<dyn IngredientFactory> {
        Box::new(AmericanIngredientFactory)
    }
}