struct SourDough;

struct ThickCrustDough;

struct ThinDough;

pub enum Dough {
    SourDoug(SourDough),
    ThickCrustDough(ThickCrustDough),
    ThinDough(ThinDough),
}

struct HotTomatoSauce;

struct MariaSauce;

struct Bechamel;

pub enum Sauce {
    HotTomatoSauce(HotTomatoSauce),
    MariaSauce(MariaSauce),
    Bechamel(Bechamel),
}

struct Cheddar;

struct Manchego;

struct Mozzarella;

pub(crate) enum Cheese {
    Cheddar(Cheddar),
    Manchego(Manchego),
    Mozzarella(Mozzarella),
}

struct Pepperoni;

struct JamonSerrano;

struct Sausage;

pub enum Meat {
    Pepperoni(Pepperoni),
    JamonSerrano(JamonSerrano),
    Sausage(Sausage),
}

struct Aubergine;

struct Pepper;

struct Onion;

struct GreenOlives;

struct Mushrooms;


pub(crate) enum Vegetable {
    Aubergine(Aubergine),
    Pepper(Pepper),
    Onion(Onion),
    GreenOlives(GreenOlives),
    Mushrooms(Mushrooms),
}

pub trait IngredientFactory {
    fn create_dough(&self) -> Dough;
    fn create_sauce(&self) -> Sauce;
    fn create_cheese(&self) -> Cheese;
    fn create_meat(&self) -> Meat;
    fn create_vegetable(&self) -> Vegetable;
}

pub struct SpanishIngredientFactory;

impl IngredientFactory for SpanishIngredientFactory {
    fn create_dough(&self) -> Dough { Dough::ThinDough(ThinDough) }
    fn create_sauce(&self) -> Sauce { Sauce::MariaSauce(MariaSauce) }
    fn create_cheese(&self) -> Cheese { Cheese::Manchego(Manchego) }
    fn create_meat(&self) -> Meat { Meat::JamonSerrano(JamonSerrano) }
    fn create_vegetable(&self) -> Vegetable { Vegetable::GreenOlives(GreenOlives) }
}

pub struct ItalianIngredientFactory;

impl IngredientFactory for ItalianIngredientFactory {
    fn create_dough(&self) -> Dough { Dough::ThinDough(ThinDough) }
    fn create_sauce(&self) -> Sauce { Sauce::HotTomatoSauce(HotTomatoSauce) }
    fn create_cheese(&self) -> Cheese { Cheese::Mozzarella(Mozzarella) }
    fn create_meat(&self) -> Meat { Meat::Pepperoni(Pepperoni) }
    fn create_vegetable(&self) -> Vegetable { Vegetable::Mushrooms(Mushrooms) }
}

pub struct AmericanIngredientFactory;

impl IngredientFactory for AmericanIngredientFactory {
    fn create_dough(&self) -> Dough { Dough::ThickCrustDough(ThickCrustDough) }
    fn create_sauce(&self) -> Sauce { Sauce::HotTomatoSauce(HotTomatoSauce) }
    fn create_cheese(&self) -> Cheese { Cheese::Cheddar(Cheddar) }
    fn create_meat(&self) -> Meat { Meat::Sausage(Sausage) }
    fn create_vegetable(&self) -> Vegetable { Vegetable::Onion(Onion) }
}