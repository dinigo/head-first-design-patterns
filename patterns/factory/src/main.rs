mod ingredients;
mod pizza;

use crate::pizza::{SpanishPizzeria, PizzaType, PizzaFactory};


fn main() {
    let spanish_pizzeria = SpanishPizzeria {};
    let _spanish_pizza = spanish_pizzeria.order_pizza(PizzaType::Cheese);
}