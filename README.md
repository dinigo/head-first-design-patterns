# Head First Design Patterns

This repo implements the patterns explained in the book.

[[_TOC_]]

## 1. Behavior Pattern

[Code](/patterns/behavior/src)

Rust is a "composition" language instead of an OOP one. This means that the
design patterns implementation will slightly vary from what you'll get in
Java/Python/C++/C#.

**Learnings**:

- Rust doesn't have inheritance, so interfaces and abstract classes are not
  available. We can implement "sort of" interfaces with traits, and then force
  some type to implement this trait.

  **For example**: we make all the ducks implement the DuckBehavior trait,
  which has the methods `perform_fly()` and `perform_quack()`. This way, even
  if the "type" of the fly_behavior and such changes, we allow it to be
  dynamic in the main.
- Inheritance is handled through instantiation instead of subclassing
- Polymorphism checks that structs implement common traits
- Individual behaviors are bound to structs and implemented using the
  corresponding common traits (Fly, Quack). This way traits ARE interfaces
- Further abstraction can be done using generics, extracting initialization to
  a trait and such. But it will obscure the example we're showing here.

## 2. Observer Pattern

[Code](/patterns/observer/src)

The whole language revolves around the concept of ownership. Rust makes sure you
share information the right way. You cannot simply pass a reference to a value
but rather you encapsulate it in a smart pointer that either:

- Box: allocates the value in the heap. Simple high level pointer
- Rc: reference counter. Allows multiple references to the same value. When the
  last reference is dropped, the value is dropped as well
- RefCell: allows to mutate the value inside

**Learnings**:

1. You can have either one mutable reference or multiple immutable references. 
   Just not both at the same time.
2. Pointers borrow the value. If you want another instance you "clone it"
   `Rc::clone(&another_rc_pointer)`
3. If you want a shared mutable ref you need both `Rc` and `RefCell` as such
    ```rust
    use std::cell::RefCell;
    use std::rc::Rc;
    
    fn main() {
        struct MyStruct { field: u16 }
        let mut my_struct = MyStruct { field: 1 };
        let mutable_ref = Rc::new(RefCell::new(my_struct));
        let copy_mutable_ref = Rc::clone(&mutable_ref);
        copy_mutable_ref.borrow_mut().field = 9;
        println!("ref_mut: {}", copy_mutable_ref.borrow().field);
    }
    ```
4. You can group traits with a super-trait like so:
    ```rust
    trait Trait1 {}
    trait Trait2 {}
    pub trait SuperTrait: Trait1 + Trait2 {}
    pub struct Struct1 {}
    impl Trait1 for Struct1 {}
    impl Trait2 for Struct1 {}
    impl SuperTrait for Struct1 {}
    pub struct Struct2 {}
    impl Trait1 for Struct2 {}
    impl Trait2 for Struct2 {}
    impl SuperTrait for Struct2 {}
    ```
   Then it's easier to have a common interface for all structs that implement it
   using `let s: Box<dyn SuperTrait> = Box::new(Struct1{});`

## 3. Decorator Pattern

[Code](/patterns/decorator/src)

This was an easy one. Only thing I'm missing is a way to extend fields so that
implementation is not so absurdly verbose.

**Learnings**:

- You might be better with structs than with enums. You can define an empty
  struct with constant fields and the declaration is more explicit than an enum

- As this is a composition pattern it's slightly easier to implement in Rust.
  Just embed your already implemented struct into other structs and "layer" the
  behavior you want.